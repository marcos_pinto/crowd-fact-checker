package br.ufrj.coppe.pesc.mturkprocessor;

public class CrowdVote
{
	public CrowdVote(String WorkerId, String HITId, String Url, String Vote, Boolean match)
	{
		this.WorkerId = WorkerId;
		this.HITId = HITId;
		this.Url = Url;
		this.Vote = Vote;
		this.Match = match;
	}
	
	private String WorkerId;
	private String HITId;			
	private String Url;			
	private String Vote; 			
	private Boolean Match;
	
	public String getWorkerId() {
		return WorkerId;
	}

	public Boolean getMatch() {
		return Match;
	}

	public String getHITId() {
		return HITId;
	}

	

	public String getUrl() {
		return Url;
	}

	

	public String getVote() {
		return Vote;
	}

	
}
