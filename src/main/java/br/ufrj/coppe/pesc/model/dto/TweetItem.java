package br.ufrj.coppe.pesc.model.dto;

public class TweetItem
{
	public TweetItem()
	{
		
	}
	public String TweetId;
	public String Title;
	public String Url;
	
	
	public String getTweetId() {
		return TweetId;
	}
	public void setTweetId(String tweetId) {
		TweetId = tweetId;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public void setUrl(String url) {
		Url = url;
	}

	
	public TweetItem(String title, String url, String tweetId) {
		Url = url;
		Title = title;
		TweetId = tweetId;
	}
	public String getTitle() {
		return Title;
	}
	
	public String getUrl() {
		return Url;
	}
	
	
}