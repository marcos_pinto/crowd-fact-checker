package br.ufrj.coppe.pesc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.ufrj.coppe.pesc.model.dto.FactCheckRequestItem;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequest;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestBatch;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestBatchRepository;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestRepository;



@RestController
@RequestMapping("/api/v1")
public class FactCheckRequestAPIController {

	 @Autowired
	private FactCheckRequestBatchRepository repository;

	
	
	 
	 @PostMapping(path = "/SalvarRequisicao", produces = "application/json", consumes = "application/json")
		@ResponseBody
		public String SalvarRequisicao(@RequestBody FactCheckRequestItem[] items)  {

		    FactCheckRequestBatch batch = new FactCheckRequestBatch();
		    batch.setCreationDate(new Date());
		 	//List<FactCheckRequest> listToSave = new ArrayList<FactCheckRequest>();
		 	
		 	String groupCode = String.valueOf(new Random().nextInt(1000000000));
			for(FactCheckRequestItem item : items)
			{
				FactCheckRequest entity = item.ToEntity(groupCode);
				entity.setBatch(batch);
				batch.getRequests().add(entity);
			}			
			
			repository.save(batch);
			repository.flush();
			
			return "Requisição criada com sucesso";	
		}
}
