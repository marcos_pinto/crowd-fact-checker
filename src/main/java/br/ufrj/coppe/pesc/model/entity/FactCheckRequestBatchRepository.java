package br.ufrj.coppe.pesc.model.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface FactCheckRequestBatchRepository extends JpaRepository<FactCheckRequestBatch, Long>{ 
	
			 
}
