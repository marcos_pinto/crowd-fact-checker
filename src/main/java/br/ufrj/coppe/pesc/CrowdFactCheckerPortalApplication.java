package br.ufrj.coppe.pesc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrowdFactCheckerPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrowdFactCheckerPortalApplication.class, args);
	}

}
