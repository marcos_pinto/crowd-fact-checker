package br.ufrj.coppe.pesc.model.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "FactCheckRequest")
public class FactCheckRequest {

	private long Id;
	private String Url;
	private String Title;
	private char PreCheck;
	private Date CreationDate;
	private String GroupCode;
	
	
	 public FactCheckRequest() {
		  
	    }
	 
	 
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
        public long getId() {
        return Id;
    }
	 
	public void setId(long id) {
	        this.Id = id;
	}
	 
	@Column(name = "Url", nullable = false)
	public String getUrl() {
		return Url;
	}


	public void setUrl(String url) {
		Url = url;
	}

	//@Lob
	@Column(name = "Title", columnDefinition = "TEXT", nullable = false)
	public String getTitle() {
		return Title;
	}


	public void setTitle(String title) {
		Title = title;
	}

	 @Column(name = "PreCheck", nullable = false)
	public char getPreCheck() {
		return PreCheck;
	}

	 @Transient
	 public String getPreCheckString()
	 {
		 return getResponseTextFromCode(getPreCheck());
	 }
	 

	public void setPreCheck(char preCheck) {
		PreCheck = preCheck;
	}

	@Column(name = "CreationDate", nullable = false)
	public Date getCreationDate() {
		return CreationDate;
	}


	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	@Column(name = "GroupCode", nullable = false)
	public String getGroupCode() {
		return GroupCode;
	}


	public void setGroupCode(String groupCode) {
		GroupCode = groupCode;
	}
	
   
    private FactCheckRequestBatch batch;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "batch_id", nullable = false)
	public FactCheckRequestBatch getBatch() {
		return batch;
	}


	public void setBatch(FactCheckRequestBatch batch) {
		this.batch = batch;
	}
	
 
	private Set<WorkerHITResponse> responses = new HashSet();

	@OneToMany(cascade = CascadeType.ALL,
	mappedBy = "request", fetch = FetchType.EAGER,
     targetEntity = WorkerHITResponse.class) 
	public Set<WorkerHITResponse> getResponses() {
		return responses;
	}

	public void setResponses(Set<WorkerHITResponse> responses) {
		this.responses = responses;
	}
		
	
	@Transient
	public String getMajorityConcensus() 
	{
		if(getResponses().size() != 0)			 
			return 
					getResponseTextFromCode(
				getResponses().stream().collect(Collectors.groupingBy(WorkerHITResponse::getResponse, Collectors.counting()))
				 .entrySet().stream().max(Map.Entry.comparingByValue())			       
			      .map(Map.Entry::getKey).orElse(null));
		else
			return "-";
				
	}
	
	@Transient
	public String getResponseTextFromCode(char responseCode)
	{
		switch(responseCode)
		{
		case 'T':
			return "True";
		case 'P':
			return "Partially-True";
		case 'N':
			return "Non-Verifiable";
		case 'F':
			return "False";
		default:
			return "-";
		}
	}
	
	@Transient
	public long countResponses(char response)
	{
		if(getResponses().size() != 0)			 
			return 
				getResponses().stream()
				.filter(x->x.getResponse() == response).count();
		else
			return 0;
	}
	
}
