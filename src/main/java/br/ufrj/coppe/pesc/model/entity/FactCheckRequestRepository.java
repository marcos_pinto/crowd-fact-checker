package br.ufrj.coppe.pesc.model.entity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface FactCheckRequestRepository extends CrudRepository<FactCheckRequest, Long>{ 
	
			
	List<FactCheckRequest> findByUrlAndBatchId(String url, long batch_id);
}
