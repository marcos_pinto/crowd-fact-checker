package br.ufrj.coppe.pesc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufrj.coppe.pesc.model.*;
import br.ufrj.coppe.pesc.model.dto.FactCheckRequestItem;
import br.ufrj.coppe.pesc.model.dto.TweetItem;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestBatch;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestBatchRepository;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestRepository;
import br.ufrj.coppe.pesc.model.entity.WorkerHITResponseRepository;
import br.ufrj.coppe.pesc.mturkprocessor.ResultProcessor;
import br.ufrj.coppe.pesc.tweeterimporter.*;
import twitter4j.TwitterException;

@Controller
public class MTurkController {

	@Autowired
	private FactCheckRequestBatchRepository repository;
	@Autowired
	private FactCheckRequestRepository requestRepository;
	@Autowired
	private WorkerHITResponseRepository hitRepository;
		
	 @GetMapping("/UploadMTurkOutput")
		public String formUploadResultados(Model model, @RequestParam("batch") int batch) {	
			model.addAttribute("batch", batch);
			return "formuploadresultados";
		}
	 
	 @PostMapping("/uploadFile")
     public String uploadFile(@RequestParam("file") MultipartFile file, HttpServletRequest request,  RedirectAttributes redirectAttributes) throws IOException, ParseException {

		 String content = new String(file.getBytes(), StandardCharsets.UTF_8);
	      
		 int batchId= (int) Integer.parseInt(request.getParameterValues("batch")[0]);
		 
		 String results = new ResultProcessor(requestRepository, hitRepository).importResults(content,batchId);
		 
	     redirectAttributes.addFlashAttribute("message",
	         "You successfully uploaded " + file.getOriginalFilename() + "!");
	
	     return "redirect:/";
	}
	 
	@RequestMapping(value = "/DownloadMTurkInput", method = RequestMethod.GET)
	 public @ResponseBody void downloadFile(@RequestParam Long batch, HttpServletResponse resp) {
	  String downloadFileName= "mturk_batch_"+ batch +"_input.txt";
	  
	  String downloadStringContent= repository.findById(batch).get().toMTurkInput();
	  try {
	   OutputStream out = resp.getOutputStream();
	   resp.setContentType("text/plain; charset=utf-8");
	   resp.addHeader("Content-Disposition","attachment; filename=\"" + downloadFileName + "\"");
	   out.write(downloadStringContent.getBytes(Charset.forName("UTF-8")));
	   out.flush();
	   out.close();

	  } catch (IOException e) {
	  }
	 }	
	
}
