package br.ufrj.coppe.pesc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.ufrj.coppe.pesc.model.*;
import br.ufrj.coppe.pesc.model.dto.FactCheckRequestItem;
import br.ufrj.coppe.pesc.model.dto.TweetItem;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestBatchRepository;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestRepository;
import br.ufrj.coppe.pesc.tweeterimporter.*;
import twitter4j.TwitterException;

@Controller
public class TweetsAtuaisController {

	@Autowired
	private FactCheckRequestBatchRepository repository;
	 
	@GetMapping("/formrequisicao")
	public String test(Model model) {	
		return "formrequisicao";
	}
	
	 @GetMapping("/listarrequisicoes")
		public String listRequests(Model model) {	
			 model.addAttribute("batches", repository.findAll());
			return "listarequisicoes";
		}
	
	@RequestMapping(path = "/GetTweets", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<TweetItem> GetTweets(@RequestParam String conta) throws TwitterException, IOException {
		
		List<TweetItem> tweets = new TwitterImporter().GetTweets(conta);
		
		return tweets;
	}
	
	
}
