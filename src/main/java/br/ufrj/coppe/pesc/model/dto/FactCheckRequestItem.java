package br.ufrj.coppe.pesc.model.dto;

import java.util.Date;
import java.util.Random;

import br.ufrj.coppe.pesc.model.entity.FactCheckRequest;

public class FactCheckRequestItem
{	
	public String Account;		
	public String Title;	
	
	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getAccount() {
		return Account;
	}

	public void setAccount(String account) {
		Account = account;
	}

	public String getTweetId() {
		return TweetId;
	}

	public void setTweetId(String tweetId) {
		TweetId = tweetId;
	}

	public String PreCheckLabel;
	public String TweetId;

	public String getUrl() {
		return "https://twitter.com/"+ Account +"/status/"+TweetId;
	}	

	public String getPreCheckLabel() {
		return PreCheckLabel;
	}

	public void setPreCheckLabel(String preCheckLabel) {
		PreCheckLabel = preCheckLabel;
	}
	
	
	public FactCheckRequest ToEntity(String groupCode)
	{
		FactCheckRequest entity = new FactCheckRequest();
		entity.setCreationDate(new Date());
		entity.setUrl(this.getUrl());
		entity.setGroupCode(groupCode);
		entity.setTitle(this.getTitle());
		entity.setPreCheck(this.getPreCheckLabel().charAt(0));
		return entity;
	}
}