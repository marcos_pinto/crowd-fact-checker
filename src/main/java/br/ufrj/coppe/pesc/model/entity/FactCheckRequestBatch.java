package br.ufrj.coppe.pesc.model.entity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import net.bytebuddy.implementation.bind.annotation.IgnoreForBinding;

@Entity
@Table(name = "FactCheckRequestBatch")
public class FactCheckRequestBatch {

	private long Id;
	private Date CreationDate;
	
	public FactCheckRequestBatch() {	  
    }
	 	 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
        public long getId() {
        return Id;
    }
	 
	public void setId(long id) {
	        this.Id = id;
	}
	 
  
   
	private Set<FactCheckRequest> requests = new HashSet();

	
  @OneToMany(cascade = CascadeType.ALL,
    fetch = FetchType.EAGER,
    mappedBy = "batch", targetEntity = FactCheckRequest.class) 
	public Set<FactCheckRequest> getRequests() {
		return requests;
	}
  
  @Transient
  public ArrayList<FactCheckRequest> getRequestsOrdered() {
	  return (ArrayList<FactCheckRequest>) getRequests().stream()
	  .sorted(Comparator.comparing(FactCheckRequest::getId)) 
	  .collect(Collectors.toList()); 
	}

	public void setRequests(Set<FactCheckRequest> requests) {
		this.requests = requests;
	}

	@Column(name = "CreationDate", nullable = false)
	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	@Transient
	public String getResumo()
	{
		return "Batch #"+ this.Id ;
	}
	
	@Transient
	public String toMTurkInput()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("title,url\r\n");
		
		for(FactCheckRequest req : getRequests())
		{
			sb.append(req.getTitle()+","+req.getUrl()+"\r\n");
		}
		return sb.toString();
	}
		
	 
}
