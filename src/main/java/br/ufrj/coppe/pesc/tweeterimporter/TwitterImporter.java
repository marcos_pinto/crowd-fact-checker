package br.ufrj.coppe.pesc.tweeterimporter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import br.ufrj.coppe.pesc.model.*;
import br.ufrj.coppe.pesc.model.dto.TweetItem;
import twitter4j.*;

	public class TwitterImporter {
	
		public List<TweetItem> GetTweets(String accountName) throws TwitterException, IOException {		
		    Twitter twitter = TwitterFactory.getSingleton();   

		    
		    int pageno = 1;
		    String user = accountName;
		    List returnList = new ArrayList<TweetItem>();
		    List statuses = new ArrayList();

		    while (true) {

		      try {

		        int size = statuses.size(); 
		        Paging page = new Paging(pageno++, 500);
		        statuses.addAll(twitter.getUserTimeline(user, page));		        
		        break;
		      }
		      catch(TwitterException e) {

		        e.printStackTrace();
		      }
		    }
 

			List<Status> tweetsPopulares = (List<Status>) statuses
		    		  .stream()
		    		  .filter(c -> (((Status)c).getRetweetCount() > 17000) 
		    				  && (((Status)c).getFavoriteCount() > 59000) 
		    				  && (!(((Status)c).getText().startsWith("RT")))
	    				  && ((((Status)c).getText().length() > 30))
	    				  )
	    		  .collect(Collectors.toList());
	    
		tweetsPopulares.sort((Status s1, Status s2) -> s2.getFavoriteCount() - s1.getFavoriteCount() );
		
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("novosTweets.csv"), "UTF-8"));
          		    
	    for (Status tweet : tweetsPopulares) {	   	
	    	String title = tweet.getText().replace(',', ';');   	
	    	String url = "https://twitter.com/"+ accountName +"/status/"+tweet.getId();		
	    	String tid = String.valueOf(tweet.getId());
	    	returnList.add(new TweetItem(title, url, tid)); 
		}
	   return returnList;
	}
	
	
}

