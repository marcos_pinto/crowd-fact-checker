package br.ufrj.coppe.pesc.mturkprocessor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import br.ufrj.coppe.pesc.model.entity.FactCheckRequest;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestBatchRepository;
import br.ufrj.coppe.pesc.model.entity.FactCheckRequestRepository;
import br.ufrj.coppe.pesc.model.entity.WorkerHITResponse;
import br.ufrj.coppe.pesc.model.entity.WorkerHITResponseRepository;
import twitter4j.*;

    

	public class ResultProcessor 
	{ 
		private HashMap<String, FactCheckRequest> requestCache = new HashMap<>();
		
		public ResultProcessor(FactCheckRequestRepository fcRepository, WorkerHITResponseRepository hitRepository)
		{
			_fcRepository = fcRepository;
			_hitRepository = hitRepository;
		}
		
		private FactCheckRequestRepository _fcRepository;
		private WorkerHITResponseRepository _hitRepository;
		
		String logResult = null;
		
		public String importResults(String resultsCsv, long batchId) throws IOException, ParseException {			
			
			List<ClaimPreCheck> precheck = readPreCheck();
			List<CrowdVote> votes = readCrowdVotes(precheck, resultsCsv, batchId);
										
			logResult += "\r\nAcertos por Trabalhador";
			
			Map<Object, Long> collect2 = votes.stream()
					.filter(c -> c.getMatch() == true) 
                    .collect(Collectors.groupingBy(o -> o.getWorkerId(), Collectors.counting()));
			 collect2.entrySet().stream().sorted(Comparator.comparing(Entry::getValue, Comparator.reverseOrder()))
			.forEach(e -> logResult += e.getKey() + " - " + e.getValue());
			 
			 return logResult;
		}	
		 
		
		private FactCheckRequest getRequestByUrlCached(String url, long batchId)
		{
			
			if(requestCache.containsKey(url))
				return requestCache.get(url);
			else
			{			
				FactCheckRequest associatedRequest = _fcRepository.findByUrlAndBatchId(url,batchId).get(0);
			
				requestCache.putIfAbsent(url, associatedRequest);
				
				return associatedRequest;
			}
		}
		
		private List<CrowdVote> readCrowdVotes(List<ClaimPreCheck> preCheck, String resultsCsv, long batchId) throws IOException, ParseException { 
			int lineNum= 0;  
			
		    List<CrowdVote> content = new ArrayList<>();
		    
		    String[] lines = resultsCsv.split("\r\n");
		        
	        for(int i=0 ; i < lines.length ; i++) 
	        { 
	        	String line = lines[i];
	        	if(lineNum++ > 0) 
	        	{
		        	String[] tokens = line.split("\",\"");
		        	String hitID = tokens[0].replaceAll ("\"","");
		        	String workerId = tokens[15].replaceAll ("\"","");
		        	String assigmentStatus = tokens[16].replaceAll ("\"","");
		        	String url = tokens[28].replaceAll ("\"","");
		        	String vote = tokens[29].replaceAll ("\"","");
		        	Date execData = new Date();//new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy").parse(tokens[12].replaceAll ("\"",""));
		        				        	
		        	if(assigmentStatus.equals("Approved") || assigmentStatus.equals("Submitted")) 
		        	{		       		
		        		
		        		
		        		
		        		
		        		WorkerHITResponse hitResponse = new WorkerHITResponse();
		        		hitResponse.setExecutionDate(execData);
		        		hitResponse.setRequest(getRequestByUrlCached(url,batchId));
		        		hitResponse.setWorkerId(workerId);
		        		hitResponse.setResponse(vote.charAt(0));
		        	
		        		_hitRepository.save(hitResponse);
		        		
			        	//Optional<ClaimPreCheck> objFound = preCheck.stream().filter(x -> x.getUrl().equals(url)).findFirst();
			        	//Boolean match = objFound.get().getVote().equals(vote);// || vote.contains(s);				        	
			        	
			        	//CrowdVote wv = new CrowdVote(workerId, hitID, url, vote, match);
			            //content.add(wv);
			        }
	            }
	        }	    
		 
		    return content;
		}
		
		private static List<ClaimPreCheck> readPreCheck() throws IOException { 		
		 
		    List<ClaimPreCheck> content = new ArrayList<>();		   
		    return content;
		}
		
	}

