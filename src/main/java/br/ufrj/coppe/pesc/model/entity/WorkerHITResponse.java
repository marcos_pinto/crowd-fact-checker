package br.ufrj.coppe.pesc.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "WorkerHITResponse")
public class WorkerHITResponse {

	private long Id;
	private FactCheckRequest request;
	private char Response;
	private Date ExecutionDate;
	private String WorkerId;
	
	
	 public WorkerHITResponse() {
		  
	    }
	 
	 
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
        public long getId() {
        return Id;
    }
	 
	public void setId(long id) {
	        this.Id = id;
	}
	 
	


    

    public char getResponse() {
		return Response;
	}


	public void setResponse(char response) {
		Response = response;
	}


	public Date getExecutionDate() {
		return ExecutionDate;
	}


	public void setExecutionDate(Date executionDate) {
		ExecutionDate = executionDate;
	}


	public String getWorkerId() {
		return WorkerId;
	}


	public void setWorkerId(String workerId) {
		WorkerId = workerId;
	}


	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "request_id", nullable = false)
	public FactCheckRequest getRequest() {
		return request;
	}


	public void setRequest(FactCheckRequest request) {
		this.request = request;
	}
 

	
}
